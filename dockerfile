# Stage 0, base on Node.js, to build and compile Angular
FROM node:12.10.0 as node

RUN chown -R /builds/lab331-2019-602115011/lab05/ 
WORKDIR /app

COPY package*.json /app/


RUN npm install

COPY ./ /app/

RUN npm run build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.13

COPY --from=node /app/dist/lab01 /usr/share/nginx/html

COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
EXPOSE 80